<?php 
/**
 * ファイルの管理
 *
 **/
function add_my_files(){
  /* --------------------------------------- */
  /* ops */
  /* --------------------------------------- */
  //- global 
  global $post;
  //- version
  $version = '20171228';
  //- テンプレートURL
  $DIR = get_template_directory_uri();


  /* --------------------------------------- */
  /* font */
  /* --------------------------------------- */
  //- noto sans
  wp_enqueue_style('noto-sans','https://fonts.googleapis.com/earlyaccess/notosansjapanese.css',null,'1','all');


  /*---------------------------------------*/
  /* styles */
  /*---------------------------------------*/
  //- bundle
  wp_enqueue_style('style.bundle.css',$DIR.'/css/style.bundle.css',null,$version,'all');


  /*---------------------------------------*/
  /* scripts */
  /*---------------------------------------*/
  //- uikit
  wp_enqueue_script('uikit.js',$DIR.'/js/uikit.min.js',null,'3.0.0-rc',true);
  wp_enqueue_script('uikit-icons.js',$DIR.'/js/uikit-icons.min.js',null,'3.0.0-rc',true);
  //- bundle
  wp_enqueue_script('app.bundle.js',$DIR.'/js/app.bundle.js',null,$version,true);


  /*---------------------------------------*/
  /* common vars */
  /*---------------------------------------*/
  wp_localize_script( 'app.bundle.js', 'wpApi',[
    // 'url' => esc_url_raw( home_url() ),
    // 'root' => esc_url_raw( rest_url() ),
    // 'dir' => esc_url_raw($DIR),
    // 'slug' => esc_html($post->post_name),
    // 'type' => esc_html($post->post_type),
    // 'id' => esc_html($post->ID),
    // 'nonce' => wp_create_nonce( 'wp_rest' )
  ]);
  wp_enqueue_script('app.bundle.js');


}
//アクションに追加
add_action('wp_enqueue_scripts','add_my_files');



/**
 * rssやwp_generator等の表記を削除
 *
 **/
function disable_worthless_tags(){
  //editURK
  remove_action('wp_head','rsd_link');
  //manifest
  remove_action('wp_head','wlwmanifest_link');
  //wp_version
  remove_action('wp_head','wp_generator');
  //emoji
  remove_action('wp_head','print_emoji_detection_script',7);
  remove_action('admin_print_scripts','print_emoji_detection_script');
  remove_action('wp_print_styles','print_emoji_styles');
  remove_action('admin_print_styles','print_emoji_styles');
  //- oembed
  remove_action('wp_head','rest_output_link_wp_head');
  remove_action('wp_head','wp_oembed_add_discovery_links');
  remove_action('wp_head','wp_oembed_add_host_js');

}
add_action('init','disable_worthless_tags');


